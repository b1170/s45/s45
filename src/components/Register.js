import { useEffect, useState } from "react"
import {
    Form,
    Button,
    Container,
    Row,
    Col
} from "react-bootstrap"



export default function Register () {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [cP, setCp] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    function registerUser(e) {
        e.preventDefault()

        setEmail("")
        setPassword("")
        setCp("")
        
        alert(`Successfully Registered`)
    }

    useEffect( () => {
        if((email !== "" && password !== "" && cP !== "") && (password === cP)) {
            setIsDisabled(false)
        }
    },  [email, password, cP])

    return (
        <Container className="my-5">
            <Row className="justify-content-center">
                <Col xs={10}
                    md={6}>
                    <Form className="border p-3" onSubmit={(e) => registerUser(e)}> 
                    
                    {/* email */}
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" value={email} 
                            onChange={(e) => {
                                setEmail(e.target.value)
                            }}
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                    {/* password */}
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => {
                                setPassword(e.target.value)
                            }}
                            />
                        </Form.Group>

                    {/* confirm password */}
                        <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control type="password"  value={cP} placeholder="Password" onChange={(e) => {
                                setCp(e.target.value)
                            }}/>
                        </Form.Group>

                        {/* submit button */}
                        <Button variant="primary" type="submit" className="mt-3" disabled={isDisabled}>
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>

    )
}
