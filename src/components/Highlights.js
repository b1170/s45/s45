import {Card, Container, Row, Col} from "react-bootstrap";

export default function Highlights () {
    return (
        <Container fluid className="my-5">
            <Row> {/* card 1 */}
                <Col xs={12}
                    md={4}>
                    <Card className="cardHighlights p-2">
                        <Card.Body>
                            <Card.Title>Learn from Home</Card.Title>
                            <Card.Text>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur magni exercitationem fugit quo sint illo illum distinctio? Expedita, aspernatur veniam.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

                {/* card 2 */}
                <Col xs={12}
                    md={4}>
                    <Card className="cardHighlights p-2">
                        <Card.Body>
                            <Card.Title>Study Now, Pay Later</Card.Title>
                            <Card.Text>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur magni exercitationem fugit quo sint illo illum distinctio? Expedita, aspernatur veniam.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

                {/* card 3 */}
                <Col xs={12}
                    md={4}>
                    <Card className="cardHighlights p-2">
                        <Card.Body>
                            <Card.Title>Be Part of Our Community</Card.Title>
                            <Card.Text>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur magni exercitationem fugit quo sint illo illum distinctio? Expedita, aspernatur veniam.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>

        </Container>
    )
}
