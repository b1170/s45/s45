import { Container, Row, Col } from "react-bootstrap";

export default function ErrorPage() {

    return(
        <Container >
            <Row className="justify-content-center mt-5">
                <Col xs={10} md={8} >
                    <div className="Jumbotron">
                        <h1>404 not found</h1>
                        <p>The page you are looking for cannoy be found</p>
                        <p>Click here go to _</p>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}