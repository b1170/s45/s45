import './App.css';
import {
    BrowserRouter as Router,
    Routes,
    Route
  } from "react-router-dom";
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home'
import Courses from './pages/Courses';
import Register from './components/Register';
import Login from './pages/Login';

import ErrorPage from './components/ErrorPage';

function App () {

    return (
        <Router>
            <AppNavbar/>
            <Routes>
                <Route path="/" element={<Home/>} />
                <Route path="/courses" element={<Courses/>} />
                <Route path="/register" element={<Register/>} />
                <Route path="/login" element={<Login/>} />
                <Route path="*" element={<ErrorPage/>} />
            </Routes>
        </Router>
    )

}


export default App;

// API State management
// API Integration
// API Integration using Fetch
// App State Management - Context (hook)