import {useEffect, useState} from "react"
import {
    Form,
    Button,
    Container,
    Row,
    Col
} from "react-bootstrap"

export default function Login () {

    const [email, setEmail] = useState ("")
    const [password, setPassword] = useState ("")
    const [isDisabled, setIsDisabled] = useState (true)

    useEffect ( () => {
        if ((email !== "" && password !== "")) {
            setIsDisabled (false)
        }
    }, [email, password])

    function loginUser (e) {
        e.preventDefault ()

        setEmail ("")
        setPassword ("")

        alert (`Successfully Logged In`)
    }


    return (
        <Container className="my-5">
        <Row className="justify-content-center">
            <Col xs={10}
                md={6}>
                    <Form onSubmit={
                        (e) => loginUser (e)
                    }>
                        <Form.Group controlId="formLoginBasicEmail" className="mb-3">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email"
                                value={email}
                                onChange={
                                    (e) => setEmail (e.target.value)
                                }/>
                        </Form.Group>

                        <Form.Group controlId="formLoginBasicPassword" className="mb-3">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password"
                                value={password}
                                onChange={
                                    (e) => setPassword (e.target.value)
                                }/>
                        </Form.Group>
                        <Button variant="primary" type="submit"
                            disabled={isDisabled}>
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}
